module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module'
  },
  env: {
    browser: true
  },
  globals: {
    angular: true
  },
  extends: 'standard',
  rules: {
    'no-unreachable': process.env.NODE_ENV === 'BUILD' ? 'error' : 'off'
  }
}
