import LoginCtrl from './login.controller'
import Routing from './login.routing'

export default angular
  .module('app.login', [])
  .config(Routing)
  .controller('LoginCtrl', LoginCtrl)
  .name
