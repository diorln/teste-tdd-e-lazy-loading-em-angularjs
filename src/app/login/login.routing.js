import loginTpl from './login.html'

let routing = function ($urlRouterProvider, $stateProvider) {
  $urlRouterProvider.otherwise('/login')

  $stateProvider
    .state('login', {
      url: '/login',
      template: loginTpl,
      controller: 'LoginCtrl',
      controllerAs: '$login'
    })
}

export default ['$urlRouterProvider', '$stateProvider', routing]
