import { Base64 } from 'js-base64'

export default ['$state', 'HttpService', 'CommonEncryptionService', class {
  constructor ($state, HttpService, CommonEncryptionService) {
    this.$state = $state
    this.HttpService = HttpService
    this.CommonEncryptionService = CommonEncryptionService
    this.username = ''
    this.password = ''
  }

  userlogin () {
    this.$state.go('index.home')
    return

    this.HttpService.ajaxPost('/login', {
      username: this.username,
      password: Base64.encode(this.CommonEncryptionService.encrypt(this.password))
    }).then((res) => {
      // response
    })
  }
}]
