import DashboardCtrl from './dashboard.controller'
import template from './dashboard.html'

let module = angular
  .module('app.index.dashboard', [])
  .controller('DashboardCtrl', DashboardCtrl)

export { module, template }
