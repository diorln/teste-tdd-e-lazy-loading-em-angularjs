
let dashboard = r => require.ensure([], () => r(require('./dashboard.module')))

let dashboardRouting = ['$stateProvider', function ($stateProvider) {
  $stateProvider
    .state('index.dashboard', {
      url: '/dashboard',
      templateProvider: ['$q', $q => {
        return $q(dashboard).then(res => res.template)
      }],
      controller: 'DashboardCtrl as $dashboard',
      resolve: {
        load: ['$q', '$ocLazyLoad', ($q, $ocLazyLoad) => {
          return $q(dashboard)
            .then(res => {
              $ocLazyLoad.load({name: res.module.name})
              return res.module
            })
        }]
      }
    })
}]

export default angular
  .module('app.index.dashboardRouting', [])
  .config(dashboardRouting)
  .name
