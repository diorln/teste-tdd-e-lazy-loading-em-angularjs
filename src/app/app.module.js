import Index from './index/index.module'
import Login from './login/login.module'

let app = () => {
  return {
    template: '<ui-view></ui-view>',
    controller: 'AppCtrl',
    controllerAs: '$app'
  }
}

class AppCtrl {
  constructor () {
    this.title = 'Here is App component'
  }
}

export default angular
  .module('app', [
    Index,
    Login
  ])
  .directive('app', app)
  .controller('AppCtrl', AppCtrl)
  .name
