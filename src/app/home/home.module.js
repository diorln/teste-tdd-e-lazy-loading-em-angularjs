
import HomeController from './home.controller'
import HomeAboutCtrl from './homeAbout.controller'

import home from './home.html'
import homeAbout from './homeAbout.html'

let module = angular
  .module('main.app.home', [])
  .controller('HomeCtrl', HomeController)
  .controller('HomeAboutCtrl', HomeAboutCtrl)

let template = {
  home,
  homeAbout
}

export { module, template }
