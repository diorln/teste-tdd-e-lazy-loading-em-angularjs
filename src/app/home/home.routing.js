
let home = r => require.ensure([], () => r(require('./home.module')))

let homeRouting = ['$urlRouterProvider', '$stateProvider', function ($urlRouterProvider, $stateProvider) {
  $urlRouterProvider.when('/home', '/home/about')

  $stateProvider
    .state('index.home', {
      url: '/home',
      templateProvider: ['$q', $q => {
        return $q(home).then(res => res.template.home)
      }],
      controller: 'HomeCtrl as $homeCtrl',
      resolve: {
        load: ['$q', '$ocLazyLoad', ($q, $ocLazyLoad) => {
          return $q(home)
            .then(res => {
              $ocLazyLoad.load({name: res.module.name})
              return res.module
            })
        }]
      }
    })
    .state('index.home.homeAbout', {
      url: '/about',
      templateProvider: ['$q', $q => {
        return $q(home).then(res => res.template.homeAbout)
      }],
      controller: 'HomeAboutCtrl as $homeAbout'
    })
    .state('index.home.homeProfile', {
      url: '/profile',
      template: `Hello! I'm Profile`,
      controller: () => {}
    })
    .state('index.home.homeMsg', {
      url: '/msg',
      template: `Hello! I'm Messages`,
      controller: () => {}
    })
}]

export default angular
  .module('app.index.homeRouting', [])
  .config(homeRouting)
  .name
