
import Routing from './index.routing.js'

import IndexCtrl from './index.controller'

import HomeRouting from '../home/home.routing'
import DashboardRouting from '../dashboard/dashboard.routing'
import ProductRouting from '../product/product.routing'

export default angular
  .module('app.index', [
    HomeRouting,
    DashboardRouting,
    ProductRouting
  ])
  .config(Routing)
  .controller('IndexCtrl', IndexCtrl)
  .name
