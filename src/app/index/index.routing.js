import indexTPL from './index.html'

let routing = function ($stateProvider) {
  $stateProvider
    .state('index', {
      abstract: true,
      template: indexTPL,
      controller: 'IndexCtrl as $index'
    })
}

export default ['$stateProvider', routing]
