
import ProductListCtrl from './productList.controller'
import ProductDetailCtrl from './productDetail.controller'

import productList from './productList.html'
import productDetail from './productDetail.html'

let module = angular
  .module('app.index.product', [])
  .controller('productListCtrl', ProductListCtrl)
  .controller('productDetailCtrl', ProductDetailCtrl)

let template = {
  productList,
  productDetail
}

export { module, template }
