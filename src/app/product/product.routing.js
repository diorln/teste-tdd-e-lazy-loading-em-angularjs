
let product = r => require.ensure([], () => r(require('./product.module')))

let productRouting = ['$stateProvider', function ($stateProvider) {
  $stateProvider
    .state('index.product', {
      abstract: true,
      template: '<ui-view></ui-view>',
      controller: () => {},
      resolve: {
        load: ['$q', '$ocLazyLoad', ($q, $ocLazyLoad) => {
          return $q(product)
            .then(res => {
              $ocLazyLoad.load({name: res.module.name})
              return res.module
            })
        }]
      }
    })
    .state('index.product.productList', {
      url: '/productList',
      templateProvider: ['$q', $q => {
        return $q(product).then(res => res.template.productList)
      }],
      controller: 'productListCtrl as $prodList'
    })
    .state('index.product.productDetail', {
      url: '/productDetail/:id',
      templateProvider: ['$q', $q => {
        return $q(product).then(res => res.template.productDetail)
      }],
      controller: 'productDetailCtrl as $prodDetail'
    })
}]

export default angular
  .module('app.index.productRouting', [])
  .config(productRouting)
  .name
