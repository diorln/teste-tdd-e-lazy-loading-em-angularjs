export default class ProductListCtrl {
  constructor () {
    this.products = [
      {
        'id': 0,
        'title': 'apple',
        'price': 900.00
      },
      {
        'id': 1,
        'title': 'banana',
        'price': 190.20
      },
      {
        'id': 2,
        'title': 'coke',
        'price': 70.00
      },
      {
        'id': 3,
        'title': 'dumpling',
        'price': 87.05
      },
      {
        'id': 4,
        'title': 'egg',
        'price': 90.60
      },
      {
        'id': 5,
        'title': 'fish',
        'price': 880.00
      }
    ]
  }
}
