import HttpService from './http.service'
import CommonEncryptionService from './commonEncryption.service'

export default angular
  .module('main.service', [])
  .service('HttpService', HttpService)
  .service('CommonEncryptionService', CommonEncryptionService)
  .name
