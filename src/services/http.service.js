export default ['$q', '$resource', 'globalConst', class {
  constructor ($q, $resource, globalConst) {
    this.$q = $q
    this.$resource = $resource
    this.globalConst = globalConst
  }

  ajaxGet (url, params) {
    var defer = this.$q.defer()
    this.$resource(this.globalConst.URLHalf + url)
      .get(params, function (res) {
        defer.resolve(res)
      }, function (err) {
        throw new Error(err.status)
      })

    return defer.promise
  }

  ajaxPost (url, params) {
    var defer = this.$q.defer()
    this.$resource(this.globalConst.URLHalf + url)
      .save(params, function (res) {
        defer.resolve(res)
      }, function (err) {
        throw new Error(err.status)
      })

    return defer.promise
  }

  ajaxDel (url, params) {
    var defer = this.$q.defer()
    this.$resource(this.globalConst.URLHalf + url)
      .delete(params, function (res) {
        defer.resolve(res)
      }, function (err) {
        throw new Error(err.status)
      })

    return defer.promise
  }

  ajaxPut (url, params) {
    var defer = this.$q.defer()
    this.$resource(this.globalConst.URLHalf + url, {}, {
      update: {
        method: 'PUT'
      }
    }).update(params, function (res) {
      defer.resolve(res)
    }, function (err) {
      throw new Error(err.status)
    })

    return defer.promise
  }
}]
