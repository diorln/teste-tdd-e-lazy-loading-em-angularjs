
export default class CommonEncryptionService {
  encrypt (str) {
    return str.split('').reverse().join('')
  }

  decrypt (str) {
    return str.split('').reverse().join('')
  }
}
