// Styles
import 'bootstrap/dist/css/bootstrap.min.css'
import './asset/sass/main.scss'

// Modules
import angular from 'angular'
import angularResource from 'angular-resource'
import uirouter from 'angular-ui-router'
import AppModule from './app/app.module'

// services
import ServiceModule from './services'

import 'babel-polyfill'

angular.module('main', [
  angularResource, uirouter, AppModule, ServiceModule,
  (() => { require('oclazyload'); return 'oc.lazyLoad' })()
])
  .constant('globalConst', {
    URLHalf: 'http://localhost:8888/app'
  })

angular.element(document).ready(() => {
  angular.bootstrap(document, ['main'])
})
