'use strict'

const path = require('path')
const baseConfig = require('./webpack.base')
const config = require('./config')
const merge = require('webpack-merge')
const webpack = require('webpack')
const chalk = require('chalk')

const devConfig = merge(baseConfig, {
  devtool: 'inline-source-map',
  devServer: {
    contentBase: path.resolve(__dirname, '../', './src'),
    /* proxy: {
      '/static': {
        target: 'http://localhost:8080',
        pathRewrite: {'^/static': '/example/static'}
      }
    }, */
    hot: true,
    publicPath: config.dev.publicPath,
    clientLogLevel: 'warning',
    host: config.dev.host,
    port: config.dev.port,
    open: config.dev.autoOpenBrowser,
    overlay: config.dev.errorOverlay
      ? { warnings: false, errors: true }
      : false,
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [{
          loader: 'style-loader'
        }, {
          loader: 'css-loader',
          options: {
            sourceMap: true
          }
        }]
      },
      {
        test: /\.scss$/,
        use: [{
          loader: 'style-loader'
        }, {
          loader: 'css-loader',
          options: {
            sourceMap: true
          }
        }, {
          loader: 'postcss-loader',
          options: {
            sourceMap: true
          }
        }, {
          loader: 'sass-loader',
          options: {
            sourceMap: true
          }
        }]
      }
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin()
  ]
})

_printBuildInfo()

function _printBuildInfo () {
  console.log('\nStarting ' + chalk.bold.green('DEV'))
  console.log('Dev server: ' + chalk.bold.yellow(config.dev.host + config.dev.port) + '\n\n')
}

module.exports = devConfig
