'use strict'

module.exports = {
  dev: {
    publicPath: '/',
    host: 'localhost',
    port: 8080,
    autoOpenBrowser: false,
    errorOverlay: true,
    useEslint: true,
    showEslintErrorsInOverlay: false,
  },
  build: {
    publicPath: '/',
    subDirectory: 'asserts/js',
    compress: false
  }
}
