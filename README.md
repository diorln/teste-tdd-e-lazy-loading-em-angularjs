# AngularJS - ocLazyLoad - Webpack

## Getting started

**1-** Install the dependecies:
```shell
npm install
```

**2-** To start the webpack-dev-server:
```shell
npm run dev
```

## Scripts

All scripts are run with `npm run [script]`, for example: `npm run dev`.

* `dev` - start development server, try it by opening `http://localhost:8080`

* `build` - create production build, check `dist` directory

* `lint` - lint code (with ESLint)
